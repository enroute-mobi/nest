# frozen_string_literal: true

RSpec.describe Nest::Version do
  describe '.current' do
    subject { Nest::Version.current }

    let(:loaded_version) { double }

    before { allow(Nest::Version).to receive(:load).and_return(loaded_version) }

    it 'returns the Version created by load method' do
      is_expected.to eq(loaded_version)
    end
  end

  describe '.load' do
    subject { Nest::Version.load }

    # rubocop:disable Style/ClassAndModuleChildren,Lint/ConstantDefinitionInBlock,RSpec/LeakyConstantDeclaration
    class self::ValidVersion
      def load; end

      def valid?
        true
      end
    end

    class self::InvalidVersion
      def load; end

      def valid?
        false
      end
    end
    # rubocop:enable Style/ClassAndModuleChildren,Lint/ConstantDefinitionInBlock,RSpec/LeakyConstantDeclaration

    def provider(valid: true)
      valid ? self.class::ValidVersion : self.class::InvalidVersion
    end

    let(:providers) { [provider(valid: false), provider] }

    before do
      allow(Nest::Version).to receive(:providers).and_return(providers)
    end

    it 'loads all new versions' do # rubocop:disable RSpec/MultipleExpectations
      # rubocop:disable RSpec/AnyInstance
      expect_any_instance_of(self.class::ValidVersion).to receive(:load)
      expect_any_instance_of(self.class::InvalidVersion).to receive(:load)
      # rubocop:enable RSpec/AnyInstance

      Nest::Version.load
    end

    it 'returns the first valid version' do
      is_expected.to be_instance_of(self.class::ValidVersion)
    end
  end

  describe '.providers' do
    subject { Nest::Version.providers }

    it { is_expected.to eq([Nest::Version::File, Nest::Version::Git, Nest::Version::Default]) }
  end

  describe Nest::Version::File do
    subject(:version) { Nest::Version::File.new }

    describe '#attributes=' do
      it 'define the name attribute' do
        expect { version.attributes = { name: 'dummy' } }.to change(version, :name).to('dummy')
      end
    end

    describe '#load' do
      subject { version.load filename }

      context 'when the given filename is a JSON file' do
        let(:filename) do
          Tempfile.new(['version', '.json']).tap do |file|
            file.write json_content
            file.close
          end.path
        end

        context 'with a name key' do
          let(:json_content) { '{"name": "test"}' }

          it { expect { version.load filename }.to change(version, :name).to('test') }
        end

        context 'without a name key' do
          let(:json_content) { '{ "dummy": "wrong" }' }

          it { expect { version.load filename }.not_to change(version, :name) }
        end

        context 'without an Hash' do
          let(:json_content) { '[ "wrong" ]' }

          it { expect { version.load filename }.not_to change(version, :name) }
        end
      end

      context 'when the given filename is nil' do
        let(:filename) { nil }

        it { expect { version.load filename }.not_to change(version, :name) }
      end

      context "when the given filename doesn't exist" do
        let(:filename) { 'dummy' }

        it { expect { version.load filename }.not_to change(version, :name) }
      end

      context 'when the given filename is not a valid JSON file' do
        let(:filename) { __FILE__ }

        it { expect { version.load filename }.to raise_error(JSON::ParserError) }
      end
    end

    describe '#default_file' do
      subject { version.default_file }

      before { allow(version).to receive(:root).and_return 'root' }

      it { is_expected.to eq('root/version.json') }
    end

    describe '#valid?' do
      subject { version.valid? }

      context 'when name is defined' do
        before { version.name = 'dummy' }

        it { is_expected.to be_truthy }
      end

      context "when name isn't defined" do
        before { version.name = nil }

        it { is_expected.to be_falsy }
      end
    end
  end

  describe Nest::Version::Git do
    subject(:version) { Nest::Version::Git.new }

    describe '#branch_name' do
      subject { version.branch_name }

      let(:git_result) { "result of 'git symbolic-ref --short HEAD'" }

      before do
        allow(version).to receive(:git).with('symbolic-ref --short HEAD').and_return(git_result)
      end

      it { is_expected.to eq(git_result) }
    end

    describe '#commit_name' do
      subject { version.commit_name }

      let(:git_result) { "result of 'git log -1 --pretty=%h'" }

      before do
        allow(version).to receive(:git).with('log -1 --pretty=%h').and_return(git_result)
      end

      it { is_expected.to eq(git_result) }
    end

    describe '#name_parts' do
      subject { version.name_parts }

      context "when branch_name is 'branch' and commit is 'commit'" do
        before do
          allow(version).to receive_messages(branch_name: 'branch', commit_name: 'commit')
        end

        it { is_expected.to eq(%w[branch commit]) }
      end

      context "when commit_name is 'commit'" do
        before { allow(version).to receive(:commit_name).and_return('commit') }

        it { is_expected.to include('commit') }
      end

      context "when commit_name is ''" do
        before { allow(version).to receive(:commit_name).and_return('') }

        it { is_expected.not_to include('') }
      end

      context 'when commit_name is nil' do
        before { allow(version).to receive(:commit_name) }

        it { is_expected.not_to include(nil) }
      end

      context "when branch_name is 'branch'" do
        before { allow(version).to receive(:branch_name).and_return('branch') }

        it { is_expected.to include('branch') }
      end

      context "when branch_name is ''" do
        before { allow(version).to receive(:branch_name).and_return('') }

        it { is_expected.not_to include('') }
      end

      context 'when branch_name is nil' do
        before { allow(version).to receive(:branch_name) }

        it { is_expected.not_to include(nil) }
      end
    end

    describe '#name' do
      subject { version.name }

      context "when branch_name is 'branch' and commit is 'commit'" do
        before do
          allow(version).to receive_messages(branch_name: 'branch', commit_name: 'commit')
        end

        it { is_expected.to eq('branch commit') }
      end

      context "when name parts are ['first','second']" do
        before do
          allow(version).to receive(:name_parts).and_return(%w[first second])
        end

        it { is_expected.to eq('first second') }
      end
    end

    describe '#load' do
      it 'ensures the name is loaded' do
        expect(version).to receive(:name)
        version.load
      end
    end

    describe '#valid?' do
      subject { version.valid? }

      context 'when name parts are empty' do
        before { allow(version).to receive(:name_parts).and_return([]) }

        it { is_expected.to be_falsy }
      end

      context 'when name parts are not empty' do
        before { allow(version).to receive(:name_parts).and_return(['dummy']) }

        it { is_expected.to be_truthy }
      end
    end
  end

  describe Nest::Version::Default do
    subject(:version) { Nest::Version::Default.new }

    context 'when current time is 2030-01-02 12:11:10' do
      let(:now) { Time.parse('2030-01-02 12:11:10') }

      before { Timecop.freeze now }

      describe '#created_at' do
        subject { version.created_at }

        it { is_expected.to eq(now) }
      end

      describe '#name' do
        subject { version.name }

        it { is_expected.to eq('20300102-121110') }
      end
    end

    describe '#valid?' do
      subject { version.valid? }

      it { is_expected.to be_truthy }
    end
  end
end
