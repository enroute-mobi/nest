# frozen_string_literal: true

# based on ruby Enumerator specs (https://github.com/ruby/ruby/tree/v2_7_8/spec/ruby/core/enumerator)

RSpec.describe Nest::NonFiberEnumerator do
  subject(:enumerator) { described_class.new(array) }

  let(:array) { [1, 2, 3] }

  it 'is enumerable' do
    expect(described_class).to include(Enumerable)
  end

  describe '#each' do
    subject { enumerator.each }

    it 'calls array #each' do
      expect(array).to receive(:each).and_return(:array_each)
      is_expected.to eq(:array_each)
    end
  end

  describe '#first' do
    subject { enumerator.first }

    it 'returns array first value' do
      is_expected.to eq(1) # will call each.first, so may allocate fibers
    end
  end

  describe '#peek' do
    subject { enumerator.peek }

    it 'returns the next element in self' do
      is_expected.to eq(1)
    end

    it 'does not advance the position of the current element' do
      expect(enumerator.next).to eq(1)
      is_expected.to eq(2)
      expect(enumerator.next).to eq(2)
    end

    it 'can be called repeatedly without advancing the position of the current element' do
      enumerator.peek
      enumerator.peek
      is_expected.to eq(1)
      expect(enumerator.next).to eq(1)
    end

    it 'works in concert with #rewind' do
      enumerator.next
      enumerator.next
      enumerator.rewind
      is_expected.to eq(1)
    end

    it 'raises StopIteration if called on a finished enumerator' do
      3.times { enumerator.next }
      expect { subject }.to raise_error(StopIteration)
    end
  end

  describe '#peek_values' do
    subject { enumerator.peek_values }

    let(:array) { [1, [2, 3], 4, [5, 6], nil, [], [nil], 9] }

    it 'returns the next element in self' do
      is_expected.to eq([1])
    end

    it 'does not advance the position of the current element' do
      expect(enumerator.next).to eq(1)
      is_expected.to eq([[2, 3]])
      expect(enumerator.next).to eq([2, 3])
    end

    it 'can be called repeatedly without advancing the position of the current element' do
      2.times { enumerator.peek_values }
      is_expected.to eq([1])
      expect(enumerator.next).to eq(1)
    end

    it 'works in concert with #rewind' do
      2.times { enumerator.next }
      enumerator.rewind
      is_expected.to eq([1])
    end

    it 'returns the next element of the enumeration' do # rubocop:disable RSpec/ExampleLength
      expect(enumerator.peek_values).to eq([1])
      enumerator.next
      expect(enumerator.peek_values).to eq([[2, 3]])
      enumerator.next
      expect(enumerator.peek_values).to eq([4])
      enumerator.next
      expect(enumerator.peek_values).to eq([[5, 6]])
      enumerator.next
      expect(enumerator.peek_values).to eq([nil])
      enumerator.next
      expect(enumerator.peek_values).to eq([[]])
      enumerator.next
      expect(enumerator.peek_values).to eq([[nil]])
      enumerator.next
      expect(enumerator.peek_values).to eq([9])
    end

    it 'raises a StopIteration exception at the end of the stream' do
      8.times { enumerator.next }
      expect { subject }.to raise_error(StopIteration)
    end
  end

  describe '#next' do
    subject { enumerator.next }

    it 'returns the next element of the enumeration' do
      expect(enumerator.next).to eq(1)
      expect(enumerator.next).to eq(2)
      expect(enumerator.next).to eq(3)
    end

    it 'raises a StopIteration exception at the end of the stream' do
      3.times { enumerator.next }
      expect { subject }.to raise_error(StopIteration)
    end

    it 'cannot be called again until the enumerator is rewound' do # rubocop:disable RSpec/ExampleLength
      3.times { enumerator.next }
      expect { enumerator.next }.to raise_error(StopIteration)
      expect { enumerator.next }.to raise_error(StopIteration)
      expect { enumerator.next }.to raise_error(StopIteration)
      enumerator.rewind
      is_expected.to eq(1)
    end
  end

  describe '#next_values' do
    subject { enumerator.next_values }

    let(:array) { [1, [2, 3], 4, [5, 6], nil, [], [nil], 9] }

    it 'returns the next element in self' do
      is_expected.to eq([1])
    end

    it 'advances the position of the current element' do
      expect(enumerator.next).to eq(1)
      is_expected.to eq([[2, 3]])
      expect(enumerator.next).to eq(4)
    end

    it 'advances the position of the enumerator each time when called multiple times' do
      2.times { enumerator.next_values }
      is_expected.to eq([4])
      expect(enumerator.next).to eq([5, 6])
    end

    it 'works in concert with #rewind' do
      2.times { enumerator.next }
      enumerator.rewind
      is_expected.to eq([1])
    end

    it 'returns the next element of the enumeration' do # rubocop:disable RSpec/ExampleLength
      expect(enumerator.next_values).to eq([1])
      expect(enumerator.next_values).to eq([[2, 3]])
      expect(enumerator.next_values).to eq([4])
      expect(enumerator.next_values).to eq([[5, 6]])
      expect(enumerator.next_values).to eq([nil])
      expect(enumerator.next_values).to eq([[]])
      expect(enumerator.next_values).to eq([[nil]])
      expect(enumerator.next_values).to eq([9])
    end

    it 'raises a StopIteration exception at the end of the stream' do
      8.times { enumerator.next }
      expect { subject }.to raise_error(StopIteration)
    end
  end

  describe '#rewind' do
    subject { enumerator.rewind }

    it 'resets the enumerator to its initial state' do
      expect(enumerator.next).to eq(1)
      expect(enumerator.next).to eq(2)
      subject
      expect(enumerator.next).to eq(1)
    end

    it 'returns self' do
      is_expected.to eq(enumerator)
    end

    it 'has no effect on a new enumerator' do
      subject
      expect(enumerator.next).to eq(1)
    end

    it 'has no effect if called multiple, consecutive times' do
      expect(enumerator.next).to eq(1)
      enumerator.rewind
      enumerator.rewind
      expect(enumerator.next).to eq(1)
    end

    it 'works with peek to reset the position' do
      enumerator.next
      enumerator.next
      subject
      enumerator.next
      expect(enumerator.peek).to eq(2)
    end
  end

  describe '#inspect' do
    subject { enumerator.inspect }

    it { is_expected.to eq('#<Nest::NonFiberEnumerator: ...>') }
  end
end
