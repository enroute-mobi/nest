# frozen_string_literal: true

RSpec.describe WelcomePhoto do
  describe '.collection' do
    subject { WelcomePhoto.collection }

    it { is_expected.to be_an_instance_of(WelcomePhoto::UnsplashCollection) }
  end

  describe '.current' do
    subject { WelcomePhoto.current }

    let(:random_photo) { instance_double(WelcomePhoto, 'Random Photo returned by the current Collection') }

    before do
      allow(WelcomePhoto).to receive_message_chain(:collection, :random).and_return(random_photo)
    end

    it { is_expected.to eq(random_photo) }
  end

  describe WelcomePhoto::UnsplashCollection do
    subject(:collection) { WelcomePhoto::UnsplashCollection.new }

    describe '#create_photo' do
      subject { collection.create_photo(unsplash_photo) }

      let(:unsplash_photo) { Unsplash::Photo.new(unsplash_attributes) }

      context 'when Unsplash Photo url regular is https://images.unsplash.com/sample' do
        let(:unsplash_attributes) { { 'urls': { 'regular': 'https://images.unsplash.com/sample' } } }

        it { is_expected.to have_attributes(image_url: a_string_starting_with('https://images.unsplash.com/sample')) }
      end

      context "when Unsplash Photo user username is 'dummy'" do
        let(:unsplash_attributes) { { 'user': { 'username': 'dummy' } } }

        it { is_expected.to have_attributes(author_link: a_string_starting_with('https://unsplash.com/@dummy')) }
      end

      context "when Unsplash Photo user name is 'Dummy'" do
        let(:unsplash_attributes) { { 'user': { 'name': 'Dummy' } } }

        it { is_expected.to have_attributes(author_name: 'Dummy') }
      end
    end

    describe '#unsplash_photos' do
      subject { collection.unsplash_photos }

      context 'when Collection is not setup successfully' do
        before { allow(collection).to receive(:setup?).and_return(false) }

        it { is_expected.to be_empty }
      end

      context 'when Collection is setup successfully' do
        before { allow(collection).to receive(:setup?).and_return(true) }

        let(:api_result) { double }

        it 'invokes Unsplash::Photo.random with count 30' do
          allow(Unsplash::Photo).to receive(:random).with(a_hash_including(count: 30)).and_return(api_result)
          is_expected.to eq(api_result)
        end

        it 'invokes Unsplash::Photo.random with collections [40395285]' do
          with_collection = a_hash_including(collections: [40_395_285])
          allow(Unsplash::Photo).to receive(:random).with(with_collection).and_return(api_result)

          is_expected.to eq(api_result)
        end
      end
    end
  end
end
