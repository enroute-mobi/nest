# frozen_string_literal: true

module Nest
  #
  # Nest::Version.current.name returns a value found either:
  # * in a /version.json file with a name entry
  # * in git branch and commit values
  # * in a fallback default version (using loading time)
  #
  class Version
    def self.current
      @current ||= load
    end

    def self.load
      providers.each do |provider|
        version = provider.new
        Nest.error.handle { version.load }
        return version if version.valid?
      end

      nil
    end

    def self.providers
      [File, Git, Default]
    end

    # Find version information in version.json file
    class File
      attr_accessor :name

      def attributes=(attributes)
        attributes.each do |k, v|
          setter = "#{k}="
          send setter, v if respond_to?(setter)
        end
      end

      def load(filename = default_file)
        return unless filename && ::File.exist?(filename)

        self.attributes = JSON.parse(::File.read(filename))
      end

      def default_file
        ::File.join(root, 'version.json')
      end

      def root
        if defined?(Rails)
          Rails.root
        else
          Dir.pwd
        end
      end

      def valid?
        name.present?
      end
    end

    # Find version information in git status
    class Git
      def branch_name
        @branch_name ||= git 'symbolic-ref --short HEAD'
      end

      def commit_name
        @commit_name ||= git 'log -1 --pretty=%h'
      end

      def git(arguments)
        `git #{arguments}`.strip
      rescue Errno::ENOENT
        # git command not found
        nil
      end

      def load
        name
      end

      def name
        @name ||= name_parts.join(' ')
      end

      def name_parts
        [branch_name, commit_name].select(&:present?)
      end

      def valid?
        name_parts.present?
      end
    end

    # Returns default version information
    class Default
      def created_at
        @created_at ||= Time.now
      end

      def name
        @name ||= created_at.strftime('%Y%m%d-%H%M%S')
      end

      def load; end

      def valid?
        true
      end
    end
  end
end
