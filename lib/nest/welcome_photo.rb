# frozen_string_literal: true

# Provides a Photo for our welcomage pages
#
# Example:
#
#   - photo = WelcomePhoto.current
#   = photo.image_url
#   = link_to photo.author_name, photo.author_link, target: "_blank"
#
class WelcomePhoto
  def self.current
    collection.random
  end

  def self.collection
    WelcomePhoto::UnsplashCollection.current
  end

  def initialize(attributes = {})
    attributes.each { |k, v| send "#{k}=", v }
  end

  attr_accessor :image_url, :author_name, :author_link

  # Retrieve photos from our unsplash collection
  # https://unsplash.com/collections/40395285/enroute
  class UnsplashCollection
    mattr_accessor :unsplash_credential
    mattr_accessor :unsplash_utm_source
    mattr_accessor :collection_id, default: 40_395_285

    def self.setup
      return false unless unsplash_credential

      Unsplash.configure do |config|
        config.application_access_key = unsplash_credential.access_key
        config.application_secret = unsplash_credential.secret_key

        config.utm_source = unsplash_utm_source
      end

      true
    end

    def self.setup?
      return @setup unless @setup.nil?

      @setup = setup
    end

    def setup?
      self.class.setup?
    end

    def self.cache
      defined?(Rails) ? Rails.cache : ActiveSupport::Cache::NullStore.new
    end

    def self.current
      cache.fetch "#{name}/current", expires_in: 1.hour do
        new.load
      end
    end

    def photos
      @photos ||= []
    end

    def random
      photos.sample
    end

    def load
      @photos = unsplash_photos.map do |unsplash_photo|
        create_photo unsplash_photo
      end
      self
    end

    def create_photo(unsplash_photo)
      WelcomePhoto.new(
        # According to documentation, the regular URL returns a 1080 photo:
        #
        # "urls": {
        #    ...
        #  "regular": "https://images.unsplash.com/face-springmorning.jpg?q=75&fm=jpg&w=1080&fit=max",
        #   ...
        # }
        #
        # See https://unsplash.com/documentation#get-a-photo
        #
        image_url: unsplash_photo.urls&.regular,
        author_name: unsplash_photo.user&.name,
        author_link: ("https://unsplash.com/@#{unsplash_photo.user.username}" if unsplash_photo.user)
      )
    end

    def unsplash_photos
      return [] unless setup?

      Unsplash::Photo.random(count: 30, collections: [collection_id])
    end
  end

  class << self
    delegate :unsplash_credential=, :unsplash_utm_source=, to: UnsplashCollection
  end
end
