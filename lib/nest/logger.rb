# frozen_string_literal: true

# :nodoc:
module Nest
  def self.logger
    @logger ||= Nest::Logger.default
  end

  # Manage Logger
  module Logger
    # Returns a Logger instance if possible Rails.logger
    def self.default
      if defined?(Rails)
        Rails.logger
      else
        require 'logger'
        ::Logger.new $stdout, formatter: ->(severity, _, _, msg) { "#{severity[0]} #{msg}\n" }
      end
    end

    # Defines #logger method, by using Nest.logger by default
    module Support
      def logger
        @logger ||= Nest.logger
      end
    end
  end
end
