# frozen_string_literal: true

# :nodoc:
module Nest
  # Manages error and associated reportings
  #
  # Similar to Rails.error but with some extensions (like message, uuid, etc)
  #
  # Usage:
  #
  #   uuid = Nest.error.report error, message: "Message"
  #
  #   Nest.error.handle do
  #     # risky code
  #   end
  #   puts "Ensured to be call"
  #
  def self.error
    @error ||= ErrorReporter.new
  end

  # Manages error and associated reportings
  #
  # Similar to ActiveSupport::ErrorReporter but with some extensions (like uuid)
  #
  class ErrorReporter
    include Nest::Logger::Support

    def initialize(*subscribers, logger: nil)
      @subscribers = subscribers.flatten
      @logger = logger
    end

    # Report an error directly to subscribers. You can use this method when the
    # block-based #handle and #record methods are not suitable.
    #
    #   uuid = error_reporter.report error, message: "Message"
    #
    # Returns uuid associated to the reported error
    def report(error, **options)
      error = Error.new(error, **options)

      subscribers.each do |subscriber|
        subscriber.report error
      rescue StandardError => e
        logger.fatal "Error subscriber raised an error: #{e.message} (#{e.class})\n" + e.backtrace.join("\n")
      end

      error.uuid
    end

    # Evaluates the given block, reporting and swallowing any unhandled error.
    # If no error is raised, returns the return value of the block. Otherwise,
    # returns the result of +fallback.call+, or +nil+ if +fallback+ is not
    # specified.
    #
    #   # Will report a TypeError to all subscribers and return nil.
    #   Rails.error.handle do
    #     1 + '1'
    #   end
    #
    # Can be restricted to handle only specific error classes:
    #
    #   maybe_tags = Rails.error.handle(Redis::BaseError) { redis.get("tags") }
    #
    # ==== Options
    #
    # * +:fallback+ - A callable that provides +handle+'s return value when an
    #   unhandled error is raised. For example:
    #
    #     user = Rails.error.handle(fallback: -> { User.anonymous }) do
    #       User.find_by(params)
    #     end
    def handle(*error_classes, fallback: nil, **options)
      error_classes = DEFAULT_RESCUE if error_classes.empty?
      yield
    rescue *error_classes => e
      report e, **options
      fallback&.call
    end

    def record(*error_classes, **options)
      error_classes = DEFAULT_RESCUE if error_classes.empty?
      yield
    rescue *error_classes => e
      report e, **options
      raise
    end

    def subscribe(subscriber)
      raise ArgumentError, 'Error subscribers must respond to #report' unless subscriber.respond_to?(:report)

      subscribers << subscriber
    end

    DEFAULT_RESCUE = [StandardError].freeze

    private

    attr_reader :subscribers
  end

  # Enhanced error
  class Error
    def initialize(error, message: nil)
      @error = error
      @message = message
    end
    attr_reader :error, :message

    # Provides an uuid to be associated with this error
    def uuid
      @uuid ||= SecureRandom.uuid
    end
  end

  module Subscriber
    # Reports error via logger (by default Nest.logger)
    class Logger
      include Nest::Logger::Support

      def initialize(logger = nil)
        @logger = logger
      end

      attr_writer :logger

      def report(error)
        logger.error message(error)
      end

      def message(e) # rubocop:disable Naming/MethodParameterName
        "[ERROR] #{e.message} (#{e.uuid}): #{e.error.class.name} #{e.error.message} #{e.error.backtrace&.join("\n")}"
      end
    end

    # Reports error via Sentry (if available)
    class Sentry
      # Returns true SENTRY_DSN env variable defined
      def self.available?
        ENV['SENTRY_DSN']
      end

      def report(error)
        return unless self.class.available?

        ::Sentry.capture_exception error.error, tags: { uuid: error.uuid }
      end
    end
  end

  error.subscribe Subscriber::Logger.new
  error.subscribe Subscriber::Sentry.new if Subscriber::Sentry.available?
end
