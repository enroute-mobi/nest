# frozen_string_literal: true

# :nodoc:
module Nest
  # Used to deprecate methods, usage, into Nest
  def self.deprecation
    @deprecation ||= ActiveSupport::Deprecation.new('next', 'Nest')
  end
end
