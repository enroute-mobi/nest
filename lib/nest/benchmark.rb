# frozen_string_literal: true

# :nodoc:
module Nest
  # =Usage:
  #
  #  Nest.benchmark.measure("test") do
  #    # process
  #  end
  def self.benchmark
    @benchmark ||= Benchmarker.new
  end

  # Provides performance and memory measurements
  class Benchmarker
    # Measure given block execution (time, memory, etc)
    def measure(_name, _attributes = {}, &block)
      # TODO
      block.call
    end

    # Returns current memory usage
    def current_memory_usage
      STATM_FOUND ? (File.read(STATM_PATH).split(' ')[1].to_i * KERNEL_PAGE_SIZE) / 1024 / 1024.0 : 0
    end

    # Returns current maps usage
    def current_map_usage
      MAPS_FOUND ? File.read(MAPS_PATH).count($RS).to_i : 0
    end

    KERNEL_PAGE_SIZE = begin
      `getconf PAGESIZE`.chomp.to_i
    rescue StandardError
      4096
    end
    STATM_PATH       = "/proc/#{Process.pid}/statm"
    STATM_FOUND      = File.exist?(STATM_PATH)

    MAPS_PATH  = "/proc/#{Process.pid}/maps"
    MAPS_FOUND = File.exist?(MAPS_PATH)
  end
end
