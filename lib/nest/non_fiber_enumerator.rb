# frozen_string_literal: true

module Nest
  # Provides Enumerator method on an Array
  #
  # Prevent Fiber usage (and associated troubles)
  #
  # :call-seq:
  #   enumerator = NonFiberEnumerator.new(array)
  #   enumerator.next
  class NonFiberEnumerator
    include Enumerable

    def initialize(array)
      @array = array
      @index = 0
    end
    attr_reader :array

    delegate :each, to: :array

    def peek
      array_end!

      array[@index]
    end

    def peek_values
      [peek]
    end

    def next
      array_end!

      result = array[@index]
      @index += 1
      result
    end

    def next_values
      [self.next]
    end

    def rewind
      @index = 0
      self
    end

    def inspect
      '#<Nest::NonFiberEnumerator: ...>'
    end

    private

    def array_length
      @array_length ||= array.length
    end

    def array_end?
      @index >= array_length
    end

    def array_end!
      raise StopIteration, 'iteration reached an end' if array_end?
    end
  end
end
